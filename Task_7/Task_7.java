package Task_7;

import java.util.Scanner;

public class Task_7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число");
        double number = scanner.nextDouble();
        if (number % 1 == 0) {
            System.out.println("Вы ввели целое число");
        } else {
            System.out.println("Вы ввели вещественное число");
        }
    }
}
