package Task_2;

public class Task_2 {
    public static void main(String[] args) {
        double[] array = {3, 5, 76, 8, 12, 10, 2};
        for (double element : array) {
            element *= 1.1;
            System.out.printf("%.2f ", element);
        }
    }
}
