package Task_11;

import java.util.Scanner;

public class Task_11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество суток");
        int days = scanner.nextInt();
        int hours = days * 24;
        int minutes = hours * 60;
        int seconds = minutes * 60;
        System.out.println(days + " суток = " + hours + " часов = " + minutes + " минут = " + seconds + " секунд");
    }
}
