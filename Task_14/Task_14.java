package Task_14;

public class Task_14 {
    public static void main(String[] args) {
        int[][] matrix = {{2, 5, 6}, {5, 3, 9}, {4, 7, 10}};
        print(matrix);
        int[][] newMatrix = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                newMatrix[i][j] = matrix[j][i];
            }
        }
        print(newMatrix);
    }
    private static void print(int[][] matrix) {
        for (int[] element : matrix) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(element[j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }
}
