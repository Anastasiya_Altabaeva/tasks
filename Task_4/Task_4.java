package Task_4;

import java.util.Scanner;

public class Task_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double SPEED_OF_SOUND = 1234.8;
        System.out.println("Введите время в секундах между вспышкой и громом");
        double time = scanner.nextDouble();
        System.out.printf("Расстояние до места удара молнии равно " + (SPEED_OF_SOUND / 3600 * time) + " км" );
    }
}
