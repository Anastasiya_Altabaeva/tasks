package Task_10;

import java.util.Scanner;

public class Task_10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку для проверки является ли она палиндромом");
        String string = scanner.nextLine();
        string = string.toLowerCase();
        string = string.replaceAll("[^a-zа-я]", "");
        while (string.isEmpty()) {
            System.out.println("Введите строку");
            string = scanner.nextLine();
            string = string.replaceAll("[^a-zа-я]", "");
        }
        if (isPalindrome(string)) {
            System.out.println("Эта строка палиндром");
        } else {
            System.out.println("Эта строка не палиндром");
        }
    }

    private static String reverseString(String string) {
        String reverseString = "";
        for (int i = string.length() - 1; i >= 0; --i) reverseString += string.charAt(i);
        return reverseString;
    }

    private static boolean isPalindrome(String string) {
        return string.equals(reverseString(string));
    }
}


