package Task_13;

import java.util.Scanner;

public class Task_13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите символ");
        char symbol = scanner.nextLine().charAt(0);
        System.out.println(info(symbol));
    }

    private static String info(char symbol) {
        if (Character.isLetter(symbol)) {
            return ("Символ является буквой");
        }
        if (Character.isDigit(symbol)) {
            return ("Символ является цифрой");
        }
        return ("Символ является знаком пунктуации");
    }
}
