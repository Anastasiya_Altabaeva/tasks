package Task_12;

import java.util.ArrayList;

public class Task_12 {
    public static void main(String[] args) {
        int[][] matrix = {{2, 5, 6}, {5, 3, 9}, {4, 7, 10}, {12, 1, 8}};
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int[] array : matrix) {
            for (int element : array) {
                arrayList.add(element);
            }
        }
        for (int number : arrayList) {
            System.out.print(number + " ");
        }
    }
}
