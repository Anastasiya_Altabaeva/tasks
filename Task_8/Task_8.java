package Task_8;

import java.util.Scanner;

public class Task_8 {
    public static void main(String[] args) {
        int[][] matrix = {{2, 5, 6}, {5, 3, 9}, {4, 7, 10}};
        Scanner scanner = new Scanner(System.in);
        print(matrix);
        System.out.println("Введите номер столбца, который хотите обнулить");
        int number = scanner.nextInt();
        matrix = set_to_zero(matrix, number - 1);
        print(matrix);
    }
    private static int[][] set_to_zero(int[][] matrix, int numberOfColumn) {
        for (int[] element : matrix) {
            element[numberOfColumn] = 0;
        }
        return matrix;
    }
    private static void print(int[][] matrix) {
        for (int[] element : matrix) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(element[j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }
}
