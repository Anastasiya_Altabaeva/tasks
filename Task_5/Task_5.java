package Task_5;

public class Task_5 {

    public static void main(String[] args) {
        System.out.print("2 3 5 7 ");
        for (int i = 9; i < 100; i += 2) {
            if ((i % 3 != 0) && (i % 5 != 0) && (i % 7 != 0)) {
                System.out.print(i + " ");
            }
        }
    }
}

