package Task_1;

import java.util.Scanner;

public class Task_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку");
        String string = scanner.nextLine();
        System.out.println("В строке " + string + " " + numberOfSpaces(string) + " пробелов");
    }
    private static int numberOfSpaces(String string) {
        int numberOfSpaces = 0;
        char[] array = string.toCharArray();
        for (char symbol : array) {
            if (symbol == ' ') {
                numberOfSpaces++;
            }
        }
        return numberOfSpaces;
    }
}
