package Task_3;

import java.util.Scanner;

public class Task_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введиту текущий курс евро = ");
        double course = scanner.nextDouble();
        System.out.println("Введие количество рублей");
        double rub = scanner.nextDouble();
        System.out.printf(rub + " рублей = " + euro(rub, course) + " евро");
    }
    private static double euro(double rub, double course) {
        return rub / course;
    }
}
