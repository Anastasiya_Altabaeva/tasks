package Task_9;

import java.util.Scanner;

public class Task_9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число");
        double number = scanner.nextDouble();
        if (isPalindrome(number)) {
            System.out.println("Это число является палиндромом");
        } else {
            System.out.println("Это число не является палиндромом");
        }
    }
    private static boolean isPalindrome(double number) {
        boolean isPalindrome = false;
        String palindrome = String.valueOf(number);
        char[] symbols = palindrome.toCharArray();
        int indexOfLeftSymbol = 0;
        int indexOfRightSymbol = (symbols.length - 1);
        while (indexOfLeftSymbol < indexOfRightSymbol) {
            if (symbols[indexOfLeftSymbol] != symbols[indexOfRightSymbol]) {
                return isPalindrome;
            } else {
                indexOfLeftSymbol++;
                indexOfRightSymbol--;
                isPalindrome = true;
            }
        }
        return isPalindrome;
    }
}
